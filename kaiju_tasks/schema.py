"""
This schema defines a task data object structure and its components.

Classes
-------

"""

import enum
import uuid
from datetime import datetime, timedelta
from typing import Union, TypedDict, Optional, List

import croniter

from kaiju_tools.rpc.jsonrpc import *
from kaiju_tools.serialization import Serializable, Hashable

__all__ = (
    'Task', 'TaskCommand', 'TaskCommandList', 'TaskStatus',
    'TemplateCategories', 'TemplateKeys'
)


class TaskStatus(enum.Enum):
    """Possible task statuses."""

    IDLE = 'idle'           #: a task has been initialized and haven't been executed
    AWAITING = 'awaiting'   #: a task is awaiting execution as soon as possible
    QUEUED = 'queued'       #: a task has been sent to an executor
    EXEC = 'executed'       #: a task has been accepted by an executor and it being executed
    ABORTED = 'aborted'     #: a task was aborted by a task manager due to some reasons
    FINISHED = 'finished'   #: a task has been finished


class TemplateCategories:
    KWS = 'KWS'             # args provided with the task
    PREVIOUS = 'PREVIOUS'   # previous stage info
    CURRENT = 'CURRENT'     # current stage info
    TASK = 'TASK'           # task information
    STAGE = 'STAGE'         # map of all stages on their numbers starting from 0


class TemplateKeys:
    CMD = 'CMD'                         # command
    RESULT = 'RESULT'                   # command result
    ERROR = 'ERROR'                     # result if it's an error
    ON_ERROR_CMD = 'ON_ERROR_CMD'       # on error command
    ON_ERROR_RESULT = 'ON_ERROR_RESULT' # on error command result


class TaskCommand(Serializable):
    """
    A single specific RPC command definition.

    Usually a task contains a command set which by itself contains a number of
    these commands in certain order. See :class:`.TaskCommandList`.

    You may specify a single command request and (optionally) a fallback hook
    if command or any command after it fails.

    .. code-block:: python

        cmd = TaskCommand(
            request={'method': 'update.something', 'params': {'id': '{kws.id}'}},
            on_failure={'method': 'delete.something': 'params': {'id': '{kws.id}'}}
        )

    :param request: RPC request method and params
    :param on_failure: RPC request if a task execution fails
    :param ignore_error: task will proceed even if this particular command fails
    """

    class Command(TypedDict):
        """Type hinting for request arguments."""

        method: str
        params: dict

    __slots__ = (
        'id', 'request', 'on_failure', 'proceed_on_errors'
    )

    IGNORE_ERROR = False

    def __init__(self, request: Command, on_failure: Command = None, ignore_error: bool = IGNORE_ERROR):

        def _prepare_request(method, params=None):
            _request = RPCRequest(method=method, params=params)
            return {'method': _request.method, 'params': _request.params}

        self.request = _prepare_request(**request)
        self.on_failure = _prepare_request(**on_failure) if on_failure else None
        self.ignore_error = bool(ignore_error)

    def repr(self):
        return {
            'request': self.request,
            'on_failure': self.on_failure,
            'ignore_error': self.ignore_error
        }


class TaskCommandList(Serializable):
    """
    A list of task RPC commands expected to be executed in a particular order.
    Each task should contain a single list of commands.

    .. code-block:: python

        instructions = TaskCommandList([
            TaskCommand({'method': 'do.something', 'params': {'id': '{kws.id}'}}),
            TaskCommand({'method': 'do.something.else', 'params': {'id': '{kws.id}'}}),
        ])

    """

    class CommandDict(TypedDict):
        """Just a type hint."""

        request: TaskCommand.Command
        on_failure: Optional[TaskCommand.Command]
        ignore_error: bool

    __slots__ = ('commands',)

    def __init__(self, commands: List[Union[CommandDict, TaskCommand]]):

        def _prepare_command(_cmd):
            if isinstance(_cmd, TaskCommand):
                return _cmd
            else:
                return TaskCommand(**_cmd)

        self.commands = [_prepare_command(cmd) for cmd in commands]

    def __iter__(self):
        return iter(self.commands)

    def repr(self):
        return {
            'commands': [
                cmd.repr() for cmd in self.commands
            ]
        }


class Task(Hashable):
    """
    A task object.

    .. note::

        Each task can be hashed by its command list and command keywords to
        prevent duplicate task creation.

    :param commands: a list of commands to be executed
    :param kws: global exec parameters (these arguments can be shared between all
        the commands and used in their parameters using the template syntax)

    :param active: inactive tasks won't be queried
    :param cron: optional CRON string
    :param start_from: optional time to start from ("do not run until")
        if None, then the task will be executed immediately or by a cron schedule
        if cron string exists
    :param max_exec_timeout: EXECUTED -> FINISHED transition of a task must take
        not more then this amount of time, otherwise it will be ABORTED, if cron
        was specified then the least between cron and max_exec_timeout values will
        be used for timeout
    :param exec_deadline: task must be FINISHED before the specified time
        otherwise it will be ABORTED, if cron was specified then this parameter
        is ignored
    :param notify: notify user when task changes its status

    :param status: task current exec status (see :class:`.TaskStatus`)
    :param created: time when task was created
    :param status_change: time the task changed its status
    :param last_run: time when the task was queued
    :param next_run: time when the task will be queued
    :param user_id: user who executed this task
    :param job_id: unique job (task execution process) identifier used to track
        ongoing task processes
    :param worker_id: task executor identifier

    :param exit_code:
    :param result:

    :param name: task name string
    :param description: task description string

    """

    __slots__ = (
        'commands', 'kws', 'callback',
        'active', 'cron', 'start_from', 'max_exec_timeout', 'exec_deadline',
        'status', 'created', 'status_change', 'last_run', 'next_run', 'notify',
        'user_id', 'job_id', 'worker_id',
        'exit_code', 'result',
        'name', 'description',
    )

    class CommandListDict:
        """Type hint."""

        commands:  List[Union[TaskCommandList.CommandDict, TaskCommand]]

    def __init__(
            self,

            commands: Union[CommandListDict, List[TaskCommand], TaskCommandList],
            kws: dict = None,
            callback: TaskCommand = None,

            # execution parameters

            active: bool = True,
            cron: str = None,
            start_from: datetime = None,
            max_exec_timeout: int = None,
            exec_deadline: datetime = None,
            notify: bool = True,

            # execution flags and metadata

            status: Union[str, TaskStatus] = TaskStatus.IDLE,

            created: datetime = None,
            status_change: datetime = None,
            last_run: datetime = None,
            next_run: datetime = None,

            user_id: uuid.UUID = None,
            job_id: uuid.UUID = None,
            worker_id: uuid.UUID = None,

            # exec results

            exit_code: int = None,
            result: dict = None,

            # mostly for convenience and notifications

            name: str = None,
            description: str = None,

            # not used

            id=None, **_):

        if isinstance(commands, (list, tuple)):
            commands = TaskCommandList(commands)
        elif isinstance(commands, TaskCommandList):
            pass
        else:
            commands = TaskCommandList(**commands)

        self.commands = commands
        self.kws = kws
        self.callback = TaskCommand(**callback) if callback else None
        self.active = active
        self.name = name if name else str(self.uuid)
        self.description = description

        if type(status) is str:
            status = TaskStatus(status)
        self.status = status

        if created is None:
            created = datetime.now()

        self.created = created

        if status_change is None:
            status_change = created

        self.status_change = status_change
        self.last_run = last_run
        self.start_from = start_from if start_from else status_change

        self.cron = cron
        if not cron:
            next_run = self.start_from
        self.next_run = next_run
        self.exec_deadline = exec_deadline
        self.max_exec_timeout = max(1, int(max_exec_timeout)) if max_exec_timeout else None

        self.user_id = user_id
        self.job_id = job_id
        self.worker_id = worker_id

        self.exit_code = exit_code
        self.result = result
        self.notify = notify

    def change_status(self, new_status: TaskStatus):
        self.status = new_status
        self.status_change = datetime.now()

    def iter(self):
        if self.cron:
            cron = croniter.croniter(self.cron, start_time=self.status_change)
            cron_iter = cron.all_next(datetime)
            t = datetime.now()
            next_run = self.next_run
            while not next_run or next_run < t:
                next_run = next(cron_iter)
            self.next_run = next_run
            self.exec_deadline = next(cron_iter)
            if self.max_exec_timeout:
                t = self.next_run + timedelta(seconds=self.max_exec_timeout)
                self.exec_deadline = min(t, self.exec_deadline)
        else:
            if not self.exec_deadline:
                if self.max_exec_timeout:
                    self.exec_deadline = datetime.now() + timedelta(seconds=self.max_exec_timeout)
                else:
                    raise RuntimeError(
                        'No task execution deadline nor execution timeout were specified.'
                        ' This is not allowed, sorry.')

    def repr(self) -> dict:
        return {

            'id': self.uuid,

            'commands': self.commands.repr(),
            'kws': self.kws,
            'callback': self.callback.repr() if self.callback else None,

            'name': self.name,
            'description': self.description,
            'status': self.status.value,
            'active': self.active,
            'cron': self.cron,

            'created': self.created,
            'status_change': self.status_change,
            'last_run': self.last_run,
            'next_run': self.next_run,
            'start_from': self.start_from,
            'exec_deadline': self.exec_deadline,

            'user_id': self.user_id,
            'job_id': self.job_id,
            'worker_id': self.worker_id,

            'notify': self.notify,
            'exit_code': self.exit_code,
            'result': self.result

        }

    @property
    def id(self):
        return self.uuid

    def _hash(self) -> dict:
        return {
            'commands': self.commands.repr(),
            'kws': self.kws,
            'callback': self.callback.repr() if self.callback else None
        }
