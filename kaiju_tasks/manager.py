"""
Task manager is a watcher what manages task statuses, sets task execution
schedule and writes the results. It also manages task locks in Redis.

.. attention::

    Currently the task manager balances tasks using Redis RANDOMKEY
    directive, i.e. it requires a clean key namespace. You should use
    a separate Redis db for the task manager otherwise it will pop random
    cache values and discard them.

The task manager has two main methods:

- **taskman.acquire** - acquires a random task (which is determined by the
    task manager's balancer) for an execution
- **taskman.write**   - writes a task result, the task must be in the EXEC
    state

Thus an executor should use *acquire* to acquire a task, perform it and
send the result to *write* or error data.

Classes
-------

"""

import asyncio
import signal
import uuid
from datetime import datetime
from typing import Union, Optional, Any, Iterable

try:
    from typing import TypedDict
except ImportError:
    # python 3.6 compatibility
    from typing_extensions import TypedDict

import sqlalchemy as sa

from kaiju_cache import RedisCache
from kaiju_db.services import SQLService
from kaiju_tools.rpc import AbstractRPCCompatible
from kaiju_tools.services import ContextableService
from kaiju_notifications import NotificationService

from .etc import *
from .models import tasks
from .schema import Task, TaskStatus

__all__ = ('TaskManager',)


class TaskManager(ContextableService, AbstractRPCCompatible):
    """
    :param app: web app
    :param database_service: database connector instance or service name
    :param cache_service: redis connector instance or service name
    :param notification_service: notification service instance or service name
    :param refresh_rate: watcher loop refresh rate in seconds
    :param notify_queued: send notifications about queued tasks
    :param notify_aborted: send notifications about aborted tasks
    :param notify_executed: send notifications about executed tasks
    :param notify_finished: send notifications about finished tasks
    :param logger: optional logger
    """

    class _H_NotifyTask(TypedDict):
        id: uuid.UUID
        name: str
        result: Optional[Any]
        user_id: Union[uuid.UUID, None]
        notify: Optional[bool]
        exit_code: Optional[int]

    service_name = 'taskman'

    ErrorCodes = ErrorCodes
    MessageCodes = MessageCodes
    cache_service_class = RedisCache
    notification_service_class = NotificationService
    REFRESH_RATE = 1
    ABORT_SIGNAL = signal.SIGINT
    table = tasks_table = tasks

    NOTIFY_QUEUED = True
    NOTIFY_ABORTED = True
    NOTIFY_EXECUTED = True
    NOTIFY_FINISHED = True

    def __init__(
            self, app,
            database_service: Union[str, SQLService.database_service_class],
            cache_service: Union[str, cache_service_class],
            notification_service: Union[str, notification_service_class],
            refresh_rate=REFRESH_RATE,
            notify_queued=NOTIFY_QUEUED,
            notify_aborted=NOTIFY_ABORTED,
            notify_executed=NOTIFY_EXECUTED,
            notify_finished=NOTIFY_FINISHED,
            permissions: dict = None,
            logger=None):

        super().__init__(app=app, logger=logger)
        AbstractRPCCompatible.__init__(self, permissions=permissions)
        self._db = self.discover_service(
            database_service, cls=SQLService.database_service_class)
        self._cache = self.discover_service(
            cache_service, cls=self.cache_service_class)
        self._notifications = self.discover_service(
            notification_service, cls=self.notification_service_class,
            required=False)

        self._notify_queued = notify_queued
        self._notify_aborted = notify_aborted
        self._notify_executed = notify_executed
        self._notify_finished = notify_finished
        self._refresh_interval = self.set_refresh_rate(refresh_rate)

        self._closing = None
        self._task = None

    @property
    def routes(self) -> dict:
        return {
            'set_refresh_rate': self.set_refresh_rate,
            'acquire': self.acquire_task,
            'write': self.write_task
        }

    @property
    def permissions(self) -> dict:
        if self._permissions:
            return self._permissions
        else:
            return {
                '*': self.PermissionKeys.GLOBAL_SYSTEM_PERMISSION
            }

    @property
    def validators(self) -> dict:
        return super().validators

    async def init(self):
        self._closing = False
        self._task = asyncio.ensure_future(self._loop())

    async def close(self):
        self._closing = True
        if self._task and not self._task.done():
            await self._task
        self._task = None
        self._closing = None

    @property
    def closed(self) -> bool:
        return self._closing is None

    async def _loop(self):
        while not self._closing:
            await self._abort_tasks_exceeded_deadline()
            await self._renew_finished_periodic_tasks()
            if not self._closing:
                await self._queue_idle_tasks()
            await asyncio.sleep(self._refresh_interval)

    def set_refresh_rate(self, rate: int):
        """Sets task manager refresh (scan) rate in seconds."""

        self._refresh_interval = max(1, int(rate))
        return self._refresh_interval

    async def acquire_task(self, worker_id: uuid.UUID):
        """
        Get a random awaiting task. This method should be used by an executor.

        :param worker_id: worker's unique identifier
        """

        task_id = await self._redis_pop_random_task()
        if task_id:
            sql = self.tasks_table.update().where(
                self.tasks_table.c.id == task_id
            ).values(
                status=TaskStatus.EXEC.value,
                status_change=datetime.now(),
                worker_id=worker_id,
                job_id=uuid.uuid4()
            ).returning(
                sa.literal_column('*')
            )
            task = await self._db.fetchrow(sql)
            if task:
                if self._notify_executed:
                    await self._send_notifications([task], self.MessageCodes.EXEC)
                return dict(task)

    async def write_task(self, id: uuid.UUID, result: Any, exit_code: int):
        """
        Write a task result in the table. This method is used by an executor.

        :param id: task id
        :param result: result value (JSON data) or error information
        :param exit_code: UNIX compatible exit code (0 for success)
        """

        sql = self.tasks_table.update().where(
            sa.and_(
                self.tasks_table.c.id == id,
                self.tasks_table.c.status == TaskStatus.EXEC.value,
                self.tasks_table.c.active == True
            )
        ).values(
            status=TaskStatus.FINISHED.value,
            status_change=datetime.now(),
            result=result,
            exit_code=exit_code
        ).returning(
            self.tasks_table.c.id,
            self.tasks_table.c.name,
            self.tasks_table.c.user_id,
            self.tasks_table.c.notify
        )
        task = await self._db.fetchrow(sql)
        if task:
            if self._notify_finished:
                task = dict(task)
                task['result'] = result
                task['exit_code'] = exit_code
                await self._send_notifications([task], self.MessageCodes.FINISHED)
            return True
        else:
            self.logger.info('The Task[%s] doesn\'t exist or it is not being executed.', id)

    async def _abort_tasks_exceeded_deadline(self):
        t = datetime.now()
        sql = self.tasks_table.update().where(
            sa.and_(
                self.tasks_table.c.exec_deadline < t,
                sa.not_(self.tasks_table.c.status.in_([
                    TaskStatus.ABORTED.value, TaskStatus.FINISHED.value
                ])),
                self.tasks_table.c.active == True,
                self.tasks_table.c.exec_deadline != None
            )
        ).values(
            status=TaskStatus.ABORTED.value,
            status_change=t,
            exit_code=128 + self.ABORT_SIGNAL.value,  # standard UNIX behavior
            result=None
        ).returning(
            self.tasks_table.c.id,
            self.tasks_table.c.name,
            self.tasks_table.c.user_id,
            self.tasks_table.c.notify,
            self.tasks_table.c.exit_code
        )
        _tasks = await self._db.fetch(sql)
        if _tasks and self._notify_aborted:
            await self._send_notifications(_tasks, self.MessageCodes.ABORT)

    async def _renew_finished_periodic_tasks(self):
        sql = self.tasks_table.select().where(
            sa.and_(
                self.tasks_table.c.status.in_([
                    TaskStatus.FINISHED.value, TaskStatus.ABORTED.value
                ]),
                self.tasks_table.c.cron != None,
                self.tasks_table.c.active == True
            )
        )
        _tasks = await self._db.fetch(sql)
        if _tasks:
            t = datetime.now()
            _tasks = (Task(**task) for task in _tasks)
            _sql = self.tasks_table.update()
            for task in _tasks:
                task.iter()
                sql = _sql.where(
                    self.tasks_table.c.id == task.id
                ).values(
                    next_run=task.next_run,
                    exec_deadline=task.exec_deadline,
                    status=TaskStatus.IDLE.value,
                    status_change=t
                )
                await self._db.execute(sql)

    async def _queue_idle_tasks(self):
        t = datetime.now()
        sql = self.tasks_table.update().where(
            sa.and_(
                self.tasks_table.c.next_run < t,
                self.tasks_table.c.status == TaskStatus.IDLE.value,
                sa.or_(
                    self.tasks_table.c.exec_deadline == None,
                    self.tasks_table.c.exec_deadline > t
                ),
                sa.or_(
                    self.tasks_table.c.start_from == None,
                    self.tasks_table.c.start_from < t
                ),
                self.tasks_table.c.active == True
            )
        ).values(
            status=TaskStatus.AWAITING.value,
            status_change=t
        ).returning(
            self.tasks_table.c.id,
            self.tasks_table.c.name,
            self.tasks_table.c.exec_deadline,
            self.tasks_table.c.user_id,
            self.tasks_table.c.notify
        )
        _tasks = await self._db.fetch(sql)
        if _tasks:
            await self._redis_set_task_locks(
                *((task['id'], task['exec_deadline']) for task in _tasks))
            if self._notify_queued:
                await self._send_notifications(_tasks, self.MessageCodes.QUEUED)

    async def _redis_set_task_locks(self, *_tasks: (uuid.UUID, datetime)):
        """
        Places TTL task locks in Redis in a transaction.
        """

        async with await self._cache.pipeline(transaction=True) as pipe:
            for task_id, deadline in _tasks:
                task_id_int = str(task_id.int)
                t = datetime.now().timestamp()
                dt = max(-1, round(deadline.timestamp() - t + 1))
                self.logger.info(
                    'Writing an awaiting Task[%s] to Redis with TTL=%d.',
                    task_id, dt)
                await pipe.set(task_id_int, None)
                await pipe.expire(task_id_int, dt)

            await pipe.execute()

    async def _redis_pop_random_task(self) -> Optional[uuid.UUID]:
        """
        Acquires a random task lock from Redis in a transaction. Returns None
        if there aren't any. This method doesn't check whether the task id is
        still valid.
        """

        async with await self._cache.pipeline(transaction=True) as pipe:
            await pipe.execute_command('RANDOMKEY')
            result = await pipe.execute()
            key = result[0]
            if key:
                await pipe.delete(key)
                await pipe.execute()
                key = uuid.UUID(int=int(key))
                self.logger.info('Popped a random Task[%s] from Redis for execution.', key)
                return key

    async def _send_notifications(self, tasks: Iterable[_H_NotifyTask], message: str):
        if self._notifications:
            _notifications = []
            for task in tasks:
                user_id = task.get('user_id')
                notify = task.get('notify')
                if user_id and notify:
                    args = {
                        'name': task['name']
                    }
                    if 'exit_code' in task:
                        args['exit_code'] = task['exit_code']
                    if 'result' in task:
                        result = []
                        for stage in task['result'].values():
                            stage = stage['RESULT']
                            if 'error' in stage:
                                result.append(stage['error'])
                            else:
                                result.append(stage['result'])
                        if len(result) == 1:
                            args['result'] = result[0]
                        else:
                            args['result'] = result
                    _notification = {
                        'task_id': task['id'],
                        'user_id': user_id,
                        'message': message,
                        'format_args': {
                            'task': args
                        }
                    }
                    _notifications.append(_notification)
            if _notifications:
                self.logger.info(
                    'Writing %d notifications with status "%s".',
                    len(_notifications), message)
                await self._notifications.m_create(data=_notifications, columns=None)
        else:
            self.logger.debug('Notifications service is disabled.')
