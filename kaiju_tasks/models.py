import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as sa_pg

__all__ = (
    'tasks', 'create_tasks_table'
)


def create_tasks_table(
        table_name: str, users_table_name: str, metadata: sa.MetaData,
        *columns: sa.Column):
    """
    :param table_name: custom table name
    :param users_table_name:
    :param metadata: custom metadata object
    :param columns: additional columns
    """

    return sa.Table(
        table_name, metadata,

        sa.Column('id', sa_pg.UUID, primary_key=True),
        sa.Column('name', sa_pg.TEXT, nullable=True),
        sa.Column('description', sa_pg.TEXT, nullable=True),
        sa.Column('commands', sa_pg.JSONB, nullable=False),
        sa.Column('callback', sa_pg.JSONB, nullable=True),
        sa.Column('kws', sa_pg.JSONB, nullable=True),
        sa.Column('cron', sa_pg.TEXT, nullable=True),

        sa.Column('active', sa_pg.BOOLEAN, nullable=False),
        sa.Column('status', sa_pg.TEXT, nullable=False),

        sa.Column('created', sa_pg.TIMESTAMP, nullable=False),
        sa.Column('status_change', sa_pg.TIMESTAMP, nullable=False),
        sa.Column('last_run', sa_pg.TIMESTAMP, nullable=True),
        sa.Column('next_run', sa_pg.TIMESTAMP, nullable=True),
        sa.Column('start_from', sa_pg.TIMESTAMP, nullable=True),
        sa.Column('exec_deadline', sa_pg.TIMESTAMP, nullable=True),

        sa.Column('user_id', sa_pg.UUID, nullable=True),

        sa.Column('notify', sa_pg.BOOLEAN, nullable=False),
        sa.Column('job_id', sa_pg.UUID, nullable=True),
        sa.Column('worker_id', sa_pg.UUID, nullable=True),
        sa.Column('exit_code', sa_pg.INTEGER, nullable=True),
        sa.Column('result', sa_pg.JSONB, nullable=True),

        *columns,

        sa.Index(f'idx_{table_name}_status', 'status'),
        sa.Index(f'idx_{table_name}_start_from', 'start_from'),
        sa.Index(f'idx_{table_name}_next_run', 'next_run'),
        sa.Index(f'idx_{table_name}_last_run', 'last_run'),
        sa.Index(f'idx_{table_name}_exec_deadline', 'exec_deadline')
    )


tasks = create_tasks_table('tasks', 'users', sa.MetaData())
