"""
Task executors execute tasks (big surprise!). They must acquire a task from
a task manager, run it and return back the result.

Classes
-------

"""

import asyncio
import errno
import random
import uuid
from typing import Union, Optional

from kaiju_tools.rpc import JSONRPCServer, RPCClientService
from kaiju_tools.rpc.abc import ServerSessionFlag
from kaiju_tools.rpc.jsonrpc import RPCError
from kaiju_tools.services import ContextableService
from kaiju_tools.mapping import fill_template

from .schema import Task, TemplateKeys, TemplateCategories
from .manager import TaskManager

__all__ = ('TaskExecutor',)


class TaskExecutor(ContextableService):
    """
    :param app: web app
    :param rpc_service: local rpc server name or instance
    :param taskman_rpc_client: rpc client service name for a task manager
    :param taskman_service_name:
    :param refresh_rate: executor refresh rate in seconds
    :param logger: optional logger instance
    """

    service_name = 'executor'

    rpc_service_class = JSONRPCServer
    taskman_rpc_client = RPCClientService
    taskman_service_name = TaskManager.service_name

    PERMISSION_DENIED_EXIT_CODE = errno.EACCES
    REFRESH_RATE = 10
    REFRESH_JITTER = REFRESH_RATE / 3

    def __init__(
            self, app,
            rpc_service: Union[str, JSONRPCServer],
            taskman_rpc_client: Union[str, taskman_rpc_client],
            taskman_service_name: str = taskman_service_name,
            refresh_rate: int = REFRESH_RATE,
            logger=None):

        super().__init__(app=app, logger=logger)
        if self.app:
            self.app_id = self.app['id']
        else:
            self.app_id = uuid.uuid4()
        self._rpc = self.discover_service(rpc_service, cls=self.rpc_service_class)
        self._taskman = self.discover_service(taskman_rpc_client)
        self._taskman_service_name = taskman_service_name
        self.refresh_rate = max(1, int(refresh_rate))
        self._task = None
        self._closing = True

    async def init(self):
        self._closing = False
        self._task = asyncio.create_task(self.run())

    async def close(self):
        self._closing = True
        await self._task

    @property
    def closed(self) -> bool:
        return self._closing

    async def run(self):
        while not self._closing:
            task = await self._acquire_task()

            if task is None:  # means that the server is closing
                return

            exit_code, result = await self._execute_task(task)
            await self._write_task(task, exit_code, result)

    async def _acquire_task(self) -> Optional[Task]:
        """
        Tries to acquire a task from a task manager. If there is nothing available
        or there was an error, it will wait for some time and will try again later.
        """

        task = None
        self.logger.debug('Acquiring a new task.')
        data = {'worker_id': self.app_id}

        while not task and not self._closing:

            if isinstance(self._taskman, TaskManager):
                task = await self._taskman.acquire_task(self.app_id)
                if task:
                    task = Task(**task)
                    return task
            else:
                try:
                    task = await self._taskman.call(f'{self._taskman_service_name}.acquire', data)
                except Exception as exc:
                    self.logger.error('Error acquiring a task. %s.', exc)
                if task:
                    task = Task(**task)
                    self.logger.info('Acquired a new %s.', task.id)
                    return task

            await asyncio.sleep(self.refresh_rate + random.random() * self.REFRESH_JITTER)

    async def _write_task(self, task: Task, exit_code: int, result):
        """
        Writes the task's result or waits until it can.

        :param task: task info
        :param exit_code: task exit status (0-127)
        :param result: task execution result or error traceback
        """

        task.result = result
        task.exit_code = exit_code
        self.logger.debug('Writing %s.', task)

        data = {
            'id': task.id,
            'result': result[TemplateCategories.STAGE],
            'exit_code': exit_code
        }

        flag = True

        while flag and not self._closing:

            if isinstance(self._taskman, TaskManager):
                await self._taskman.write_task(task.id, result, exit_code)
                if task.callback:
                    request = fill_template(task.callback.request, result, default=None)
                    callback = await self._rpc.call(request, {})
                    self.logger.debug(callback)
                flag = False
            else:
                try:
                    result = await self._taskman.call(f'{self._taskman_service_name}.write', data)
                except Exception as exc:
                    self.logger.error('Error writing %s. %s. Retrying.', task, exc)
                    await asyncio.sleep(self.refresh_rate + random.random() * self.REFRESH_JITTER)
                else:
                    if task.callback:
                        request = fill_template(task.callback.request, result, default=None)
                        callback = await self._rpc.call(request, {})
                        self.logger.debug(callback)
                    flag = False

    def _get_headers(self, task):
        headers = {
            JSONRPCServer.APP_ID_HEADER: self.app_id,
            JSONRPCServer.DEADLINE_HEADER: int(task.exec_deadline.timestamp()) + 1
        }
        return headers

    async def _execute_task(self, task: Task):

        def _has_errors(_result):
            if isinstance(_result, list):
                for value in _result:
                    if isinstance(value, RPCError):
                        return True
            elif isinstance(_result, RPCError):
                return True
            else:
                return False

        self.logger.debug('Running Task[%s].', task.id)

        data = {
            TemplateCategories.TASK: task.repr(),
            TemplateCategories.KWS: task.kws,
            TemplateCategories.PREVIOUS: {},
            TemplateCategories.CURRENT: {},
            TemplateCategories.STAGE: {}
        }

        exit_code = 0

        for cmd_id, cmd in enumerate(task.commands):

            self.logger.debug('Executing stage %d.', cmd_id)

            cmd_id_str = str(cmd_id)
            data[TemplateCategories.PREVIOUS] = data[TemplateCategories.CURRENT]
            data[TemplateCategories.STAGE][cmd_id_str] = data[TemplateCategories.CURRENT] = stage = {}

            request = fill_template(cmd.request, data, default=None)
            stage[TemplateKeys.CMD] = request
            headers = self._get_headers(task)
            _h, result = await self._rpc.call(request, headers, session=ServerSessionFlag)  # TODO: user sessions
            stage[TemplateKeys.RESULT] = result

            if cmd.on_failure:
                on_failure = fill_template(cmd.on_failure, data, default=None)
                stage[TemplateKeys.ON_ERROR_CMD] = on_failure
            else:
                stage[TemplateKeys.ON_ERROR_CMD] = None
                stage[TemplateKeys.ON_ERROR_RESULT] = None

            if _has_errors(result):
                if not cmd.ignore_error:
                    exit_code = 1
                    for command_id, command in data[TemplateCategories.STAGE].items():
                        on_failure = command[TemplateKeys.ON_ERROR_CMD]
                        if on_failure:
                            _h, result = await self._rpc.call(on_failure, headers, session=ServerSessionFlag)  # TODO: user sessions
                            command[TemplateKeys.ON_ERROR_RESULT] = result
                    break

        return exit_code, data
