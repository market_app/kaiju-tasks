import pytest

from kaiju_tools.tests.fixtures import *
from kaiju_tools.rpc.tests.fixtures import *
from kaiju_db.tests.fixtures import *
from kaiju_cache.tests.fixtures import *


@pytest.fixture
def simple_task():
    return {
        'commands': [
            {
                'request': {
                    'method': 'm.test',
                    'params': {'id': '[KWS.item_id]', 'value': 1000}
                }
            }
        ],
        'kws': {
            'item_id': 42
        },
        'callback': {
            'request': {
                'method': 'm.notify',
                'params': {
                    'task_id': '[TASK.id]'
                }
            }
        }
    }
