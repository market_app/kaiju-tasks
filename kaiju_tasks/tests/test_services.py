import asyncio

import pytest

from kaiju_tools.rpc import JSONRPCServer, RPCClientService, AbstractRPCCompatible
from kaiju_tools.exceptions import NotFound, Conflict

from kaiju_notifications.services import NotificationService

from .fixtures import *
from ..schema import TaskStatus
from ..services import TaskService, TaskManager, TaskExecutor


async def test_task_interface(
        database, database_service, admin_session, user_session, simple_task, logger):

    interface = TaskService(app=None, database_service=database_service, logger=logger)

    async with database_service:
        session = admin_session
        task = await interface.create(simple_task, session=session)
        task_id = task['id']

        logger.info('Testing task aborting')
        await interface.abort(task_id, session=session)

        logger.info('Task shouldn\'t be able to be aborted twice')
        with pytest.raises(Conflict):
            await interface.abort(task_id, session=session)

        logger.info('Testing task restart.')
        await interface.restart(task_id, session=session)

        logger.info('Task shouldn\'t be able to be restarted twice')
        with pytest.raises(Conflict):
            await interface.restart(task_id, session=session)

        await interface.update(task_id, {'active': False}, session=session)

        logger.info('Inactive tasks can\'t be aborted or restarted.')
        with pytest.raises(Conflict):
            await interface.abort(task_id, session=session)

        logger.info('Non-admin user shouldn\'t be able to view or change tasks owned by another user.')
        with pytest.raises(NotFound):
            await interface.abort(task_id, session=user_session)


async def test_task_manager(
        database, redis, database_service, redis_cache, simple_task, user_session, logger):

    interface = TaskService(
        app=None, database_service=database_service, logger=logger)
    notification_service = NotificationService(
        app=None, database_service=database_service, logger=logger)
    task_man = TaskManager(
        app=None, database_service=database_service, refresh_rate=0.1,
        cache_service=redis_cache, notification_service=notification_service,
        logger=logger)

    async with database_service:
        async with task_man:
            task = await interface.create(simple_task, session=user_session)
            task_id = task['id']
            await asyncio.sleep(1)
            task = await interface.get(task_id, session=user_session)
            assert task['status'] == TaskStatus.AWAITING.value
            worker_id = uuid.uuid4()
            task = await task_man.acquire_task(worker_id)
            task_id = task['id']
            task = await interface.get(task_id, session=user_session)
            assert task['status'] == TaskStatus.EXEC.value
            assert task['worker_id'] == worker_id
            result = 42
            await task_man.write_task(task_id, result=42, exit_code=0)
            task = await interface.get(task_id, session=user_session)
            assert task['result'] == result
            assert task['exit_code'] == 0
            assert task['status'] == TaskStatus.FINISHED.value


async def test_task_executor(
        database, redis, database_service, redis_cache, simple_task,
        user_session, logger):

    class SomeService(AbstractRPCCompatible):
        service_name = 'm'

        def __init__(self, *_, **__):
            self.task = None

        @property
        def routes(self) -> dict:
            return {
                'test': self.test,
                'notify': self.notify
            }

        def test(self, id, value):
            return id * value

        def notify(self, task_id):
            self.task = task_id

    interface = TaskService(
        app=None, database_service=database_service, logger=logger)
    notification_service = NotificationService(
        app=None, database_service=database_service, logger=logger)
    task_man = TaskManager(
        app=None, database_service=database_service, refresh_rate=1,
        cache_service=redis_cache, notification_service=notification_service,
        logger=logger)
    rpc_server = JSONRPCServer(
        app=None,
        guest_permission_key=AbstractRPCCompatible.PermissionKeys.GLOBAL_SYSTEM_PERMISSION,
        debug=True, logger=logger)
    task_executor = TaskExecutor(
        app=None, rpc_service=rpc_server, taskman_rpc_client=task_man,
        refresh_rate=1, logger=logger)

    service = SomeService()

    rpc_server.register_service(interface.service_name, interface)
    rpc_server.register_service(task_man.service_name, task_man)
    rpc_server.register_service(notification_service.service_name, notification_service)
    rpc_server.register_service(service.service_name, service)

    async with database_service:
        async with rpc_server:

            async with task_man:
                task = await interface.create(simple_task, session=user_session)
                logger.debug(task)
                task_id = task['id']
                async with task_executor:
                    await asyncio.sleep(5)

            assert service.task == task_id
