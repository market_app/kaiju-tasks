"""
This package contains all services required for program tasks management.

This diagram shows service relations and dependencies:

.. image:: ../images/tasks.png

"""

from .etc import *
from .models import *
from .schema import *
from .services import *

__version__ = '0.1.0'
__python_version__ = '3.8'
__author__ = 'antonnidhoggr@me.com'
