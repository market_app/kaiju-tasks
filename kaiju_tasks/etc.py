"""
Variables and structures used by other classes.
"""

from kaiju_db.services import SQLService

__all__ = (
    'Permissions', 'ErrorCodes', 'MessageCodes'
)


class Permissions:
    """Specific permission keys used by task services."""

    VIEW_OTHERS_TASKS = 'tasks.view_other_tasks'
    MODIFY_OTHERS_TASKS = 'tasks.modify_others_tasks'


class ErrorCodes(SQLService.ErrorCodes):
    """Error codes used by task services."""

    INVALID_TASK = 'tasks.invalid_task'
    STATUS_NOT_ALLOWED = 'tasks.status_change_not_allowed'
    NOT_ACTIVE = 'tasks.not_active'
    LOCKED = 'tasks.locked'


class MessageCodes:
    """Message codes used by task services."""

    QUEUED = 'tasks.message.queued'
    ABORT = 'tasks.message.aborted'
    ERROR = 'tasks.message.error'
    EXEC = 'tasks.message.exec'
    FINISHED = 'tasks.message.finished'
