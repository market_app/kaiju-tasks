"""
The :class:`.TaskService` is an SQL interface which manages task records.
It doesn't execute or queue tasks, but provides an RPC interface for creating
or querying task information or changing task status.

Classes
-------

"""

import uuid
from datetime import timedelta, datetime
from typing import Union

import sqlalchemy as sa

from kaiju_db.services import SQLService
from kaiju_tools.exceptions import ValidationError, Conflict
from kaiju_tools.rpc import AbstractRPCCompatible
from kaiju_tools.rpc.abc import ServerSessionFlag, Session

from .etc import ErrorCodes, Permissions
from .models import tasks
from .schema import Task, TaskStatus

__all__ = ('TaskService',)


class TaskService(SQLService, AbstractRPCCompatible):
    """
    RPC interface for task management.

    Task interface allows you to change task status manually, view, create, delete
    or modify tasks.
    """

    service_name = 'tasks'

    ErrorCodes = ErrorCodes
    Permissions = Permissions
    table = tasks
    DEFAULT_EXEC_TIME = 60  # default max task execution time (sec)
    MAX_EXEC_TIME = 600  # max allowed task execution time (sec)

    insert_columns = {
        'commands', 'kws', 'callback',
        'cron', 'exec_deadline', 'name', 'description', 'start_from',
        'max_exec_timeout', 'notify'
    }
    update_columns = {
        'name', 'description', 'cron', 'active', 'start_from', 'exec_deadline',
        'max_exec_timeout', 'notify'
    }

    def __init__(
            self, app, database_service: Union[str, SQLService.database_service_class],
            max_exec_time=MAX_EXEC_TIME,
            default_exec_time=DEFAULT_EXEC_TIME,
            permissions: dict = None,
            logger=None):

        super().__init__(app=app, database_service=database_service, logger=logger)
        AbstractRPCCompatible.__init__(self, permissions=permissions)
        self.max_exec_time = max(1, int(max_exec_time))
        self.default_exec_time = max(1, int(default_exec_time))
        self._max_exec_time = timedelta(seconds=self.max_exec_time)
        self._default_exec_time = timedelta(seconds=self.default_exec_time)

    @property
    def routes(self) -> dict:
        return {
            **super().routes,
            'restart': self.restart,
            'abort': self.abort
        }

    @property
    def permissions(self) -> dict:
        if self._permissions:
            return self._permissions
        else:
            return {
                '*': self.PermissionKeys.GLOBAL_USER_PERMISSION
            }

    @property
    def validators(self) -> dict:
        return super().validators

    def _prepare_deadline(self, data: dict):
        deadline = data.get('exec_deadline')
        timeout = data.get('max_exec_timeout')
        if not deadline and not timeout:
            data['exec_deadline'] = datetime.now() + self._default_exec_time

    def prepare_insert_data(self, session, data: dict):
        data['user_id'] = self.get_user_id(session)
        self._prepare_deadline(data)
        try:
            task = Task(**data)
            task.iter()
        except (KeyError, ValueError, TypeError, RuntimeError) as exc:
            raise ValidationError(
                'Invalid task parameters.',
                base_exc=exc, service=self.service_name,
                code=self.ErrorCodes.INVALID_TASK)
        else:
            return task.repr()

    def prepare_update_data(self, session, data: dict):
        self._prepare_deadline(data)
        return data

    def _place_user_condition(self, sql, session, permission):
        """Places user condition if a user has no admin/system privileges."""

        if not self.has_permission(session, permission):
            user_id = self.get_user_id(session)
            sql = sql.where(self.table.c.user_id == user_id)
        return sql

    def _get_condition_hook(self, sql, session: Union[Session, ServerSessionFlag]):
        return self._place_user_condition(sql, session, self.Permissions.VIEW_OTHERS_TASKS)

    def _update_condition_hook(self, sql, session: Union[Session, ServerSessionFlag]):
        return self._place_user_condition(sql, session,  self.Permissions.MODIFY_OTHERS_TASKS)

    def _delete_condition_hook(self, sql, session: Union[Session, ServerSessionFlag]):
        return self._update_condition_hook(sql, session)

    async def _raise_status_error(self, _id, session):
        """This function raises a specific error (NotFound or Conflict) depending
        on which condition has failed."""

        task = await self.get(_id, columns=['status', 'active'], session=session)
        if not task['active']:
            raise Conflict(
                'Task is not active.',
                service=self.service_name,
                task_id=_id, code=self.ErrorCodes.NOT_ACTIVE)
        else:
            raise Conflict(
                'Task can\'t be aborted because it\'s not idle.',
                service=self.service_name,
                task_id=_id, task_status=task['status'],
                code=self.ErrorCodes.STATUS_NOT_ALLOWED)

    async def restart(self, id: uuid.UUID, exec_deadline: datetime = None, session=ServerSessionFlag):
        """
        Restarts a task if possible.

        The task must be active, and not running, executing or awaiting execution,
        and the user should be able to modify the task.
        """
        if exec_deadline is None:
            exec_deadline = datetime.now() + timedelta(days=1)

        sql = self.table.update().values({
            'next_run': datetime.now(),
            'exec_deadline': exec_deadline,
            'status': TaskStatus.IDLE.value
        }).where(
            sa.and_(
                self.table.c.id == id,
                self.table.c.status.in_([TaskStatus.ABORTED.value, TaskStatus.FINISHED.value]),
                self.table.c.active == True
            )
        )
        sql = self._place_user_condition(sql, session, self.Permissions.MODIFY_OTHERS_TASKS)
        sql = sql.returning(self.table.c.id)
        result = await self._wrap_update(self._db.fetchrow(sql))

        if not result:
            await self._raise_status_error(id, session)

        return {
            'id': id,
            'status': TaskStatus.IDLE.value
        }

    async def abort(self, id: uuid.UUID, session=ServerSessionFlag):
        """
        Aborts a task execution.

        The task must be in an idle status and must be visible by the user.
        """

        sql = self.table.update().values({
            'status': TaskStatus.ABORTED.value
        }).where(
            sa.and_(
                self.table.c.id == id,
                self.table.c.status == TaskStatus.IDLE.value,
                self.table.c.active == True
            )
        )
        sql = self._place_user_condition(sql, session, self.Permissions.MODIFY_OTHERS_TASKS)
        sql = sql.returning(self.table.c.id)
        result = await self._wrap_update(self._db.fetchrow(sql))

        if not result:
            await self._raise_status_error(id, session)

        return {
            'id': id,
            'status': TaskStatus.ABORTED.value
        }
